package ru.alex.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alex.entity.AppPhoto;

public interface AppPhotoDAO extends JpaRepository<AppPhoto, Long> {
}
