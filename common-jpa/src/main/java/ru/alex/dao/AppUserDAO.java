package ru.alex.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alex.entity.AppUser;
@Repository
public interface AppUserDAO extends JpaRepository<AppUser, Long> {
    AppUser findUserByTelegramId(Long id);
}
