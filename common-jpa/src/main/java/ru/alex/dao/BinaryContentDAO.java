package ru.alex.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alex.entity.BinaryContent;

public interface BinaryContentDAO extends JpaRepository<BinaryContent, Long> {
}
