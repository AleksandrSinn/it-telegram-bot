package ru.alex.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.alex.service.UpdateProducer;
import ru.alex.utils.MessageUtils;

import java.util.Objects;

import static ru.alex.model.RabbitQueue.*;

@Component
@Log4j
@RequiredArgsConstructor
public class UpdateController {
    private TelegramBot telegramBot;
    private final MessageUtils messageUtils;
    private final UpdateProducer updateProducer;

    public void registerBot(TelegramBot telegramBot){
        this.telegramBot = telegramBot;
    }
    public void processUpdate(Update update){
        if(update.hasMessage()){
            distributeMessagesByType(update);
        }else{
            log.error("Received unsupported message type "+ update);
        }
    }
    private void distributeMessagesByType(Update update) {
        var message = update.getMessage();
        if(message.hasText()){
            log.debug("Text message process start");
            processTextMessage(update);
        }else if(message.hasDocument()){
            log.debug("Documents message process start");
            processDocMessage(update);
        }else if (message.hasPhoto()){
            log.debug("Photo message process start");
            processPhotoMessage(update);
        }else{
            log.debug("Unsupported Message Type process start");
            setUnsupportedMessageTypeView(update);
        }
    }

    public void setUnsupportedMessageTypeView(Update update) {
        var sendMessage = messageUtils.generateSendMessageWithText(update, "Unsupported message type");
        setView(sendMessage);
    }

    public void setView(SendMessage sendMessage) {
        telegramBot.sendAnswerMessage(sendMessage);
    }

    private void processPhotoMessage(Update update) {
        updateProducer.produce(PHOTO_MESSAGE_UPDATE, update);
        setFileIsReceivedView(update);
    }

    private void setFileIsReceivedView(Update update) {
        var sendMessage = messageUtils.generateSendMessageWithText(update, "File is added! in progress");
        setView(sendMessage);
    }

    private void processDocMessage(Update update) {
        updateProducer.produce(DOC_MESSAGE_UPDATE, update);
    }

    private void processTextMessage(Update update) {
        updateProducer.produce(TEXT_MESSAGE_UPDATE, update);
    }
}
