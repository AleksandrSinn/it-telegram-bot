package ru.alex.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alex.entity.RawData;

public interface RawDataDAO extends JpaRepository<RawData, Long> {
}
