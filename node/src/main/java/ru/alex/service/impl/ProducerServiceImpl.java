package ru.alex.service.impl;

import org.jvnet.hk2.annotations.Service;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import ru.alex.service.ProducerService;

import static ru.alex.model.RabbitQueue.ANSWER_MESSAGE;

@Service
record ProducerServiceImpl(
        RabbitTemplate rabbitTemplate) implements ProducerService {
    @Override
    public void producerAnswer(SendMessage sendMessage) {
        rabbitTemplate.convertAndSend(ANSWER_MESSAGE, sendMessage);
    }
}
